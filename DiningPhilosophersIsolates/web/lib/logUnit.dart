library logUnit;

import 'dart:html';

//For the debug on browser
class LogUnit {
  DivElement _div;
  TextAreaElement _console;

  LogUnit(this._div, {int cols, int rows}) {
    _console = new TextAreaElement()
        ..attributes['cols'] = cols != null ? 'cols' : '50'
        ..attributes['rows'] = rows != null ? 'rows' : '10'
        ..attributes['readonly'] = 'true'
        ..attributes['disabled'] = 'true';
    _div.append(_console);
  }

  appendDebugMessage(String message) {
    _console.appendText(message);
    _console.appendText('\n');
    //'autoscroll' the new messages
    _console.scrollTop = _console.scrollHeight;
  }
}