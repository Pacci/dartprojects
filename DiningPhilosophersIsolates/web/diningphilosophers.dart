import 'dart:html';
import 'dart:async';
import 'dart:isolate';
import 'dart:math';
import 'lib/logUnit.dart';

final rng = new Random();

final showLog = true;
LogUnit _log;

ButtonElement startPhilo;
ButtonElement stopPhilo;

CanvasElement canvasElement;
CanvasRenderingContext2D ctx2D;

ReceivePort receivePort;
StreamSubscription msgSubscription;

Uri uri_isolate;
ImageElement image;

//Simulation variables
var NPhilo = 5;
List<Philosopher> philosophers;
var availForks;
List<Request> pendingRequest;

//Waiter
main() {
  uri_isolate = new Uri.file('isolatephilosopher.dart');
  image = new ImageElement(src: "img/background.png");
  //Get the reference to the element in the DOM.
  startPhilo = querySelector('#startDiningPhilosophers');
  stopPhilo = querySelector('#stopDiningPhilosophers');
  canvasElement = querySelector('#philoImage');
  ctx2D = canvasElement.context2D;
  //Get the EventStream<MouseEvent> and add a subscription
  //On each data event from this stream,
  //the subscriber's [startPhiloIsolates] handler is called.
  image.onLoad.listen((_) => initialize());

  //Enable startPhilo button and disable stopPhilo button
  startPhilo.disabled = false;
  stopPhilo.disabled = true;
  
  startPhilo.onClick.listen(startPhiloIsolates);
  stopPhilo.onClick.listen(stopPhiloIsolates);

  //Log
  if (showLog) _log = new LogUnit(querySelector('#debugText'));
}

stopPhiloIsolates(Event e) {
  String msg = "End of simulation!";
  showLog ? _log.appendDebugMessage(msg) : print(msg);
  msgSubscription.cancel();
  Future.forEach(philosophers, (Philosopher philo) => philo.changeState("Stop"))
  .whenComplete(() {
    //Enable startPhilo button and disable stopPhilo button
    startPhilo.disabled = false;
    stopPhilo.disabled = true;
    
    scheduleMicrotask(redraw);
  });
}

startPhiloIsolates(Event e) {
  //Disable startPhilo button and enable stopPhilo button
  startPhilo.disabled = true;
  stopPhilo.disabled = false;

  //Opens a port for receiving messages.
  receivePort = new ReceivePort();
  msgSubscription = receivePort.listen(handleMessage);

  //Initialize some variables
  philosophers = [new Philosopher("Socrate"), new Philosopher("Platone"), new Philosopher("Aristotele"), new Philosopher("Talete"), new Philosopher("Me")];
  availForks = [true, true, true, true, true];
  pendingRequest = new List<Request>();

  for (int i = 0; i < NPhilo; i++) {
    //Init state for philosophers
    philosophers[i].changeState("Init");
    /*
     * Generate NPhilo isolate with args[0] = philoID and args[1] = total number of philo
     * with a msg which is the sendPort for the Waiter
     */
    Isolate.spawnUri(uri_isolate, ['$i', '$NPhilo'], receivePort.sendPort);
  }
  scheduleMicrotask(redraw);
}

handleMessage(var msg) {
  //This is the first message send by the isolate at the start
  if (msg[0] == "Ready") {
    //Log
    showLog ? _log.appendDebugMessage(msg[1]) : print(msg[1]);

    //Send back the message
    SendPort reply = msg[2];
    new Future.delayed(new Duration(seconds: 5), () => reply.send("Start"));

  } else if (msg[0] == "Think") {
    //Set the data received
    var id = msg[1],
        nSeconds = msg[2];

    //Log
    String s = "Philosopher ${philosophers[int.parse(id)].name} is thinking for $nSeconds seconds!";
    showLog ? _log.appendDebugMessage(s) : print(s);

    //Reply to stop after nSeconds
    SendPort reply = msg[3];
    new Future.delayed(new Duration(seconds: nSeconds), () => reply.send("Stop"));

    //Update image
    philosophers[int.parse(id)].changeState("Think");
    scheduleMicrotask(redraw);

  } else if (msg[0] == "WantsToEat") {
    var id = msg[1],
        indexFirstFork = msg[2],
        indexSecondFork = msg[3];
    SendPort reply = msg[4];

    //Log
    String s = "Philosopher ${philosophers[int.parse(id)].name} wants to eat!";
    showLog ? _log.appendDebugMessage(s) : print(s);

    if (availForks[indexFirstFork] == true && availForks[indexSecondFork] == true) {
      availForks[indexFirstFork] = false;
      availForks[indexSecondFork] = false;

      //Reply forks are available
      reply.send("ForkAvailable");
    } else {
      //Add the request in the queue
      pendingRequest.add(new Request(reply, id, indexFirstFork, indexSecondFork));
    }
  } else if (msg[0] == "Eat") {
    var id = msg[1],
        nSeconds = msg[2],
        indexFirstFork = msg[3],
        indexSecondFork = msg[4];
    SendPort reply = msg[5];

    //Log
    String s = "Philosopher ${philosophers[int.parse(id)].name} is eating for $nSeconds!";
    showLog ? _log.appendDebugMessage(s) : print(s);

    //Update image
    philosophers[int.parse(id)].changeState("Eat");
    scheduleMicrotask(redraw);

    new Future.delayed(new Duration(seconds: nSeconds), () {
      //Reply to stop
      reply.send("Stop");
      //After nSeconds the philo stops to eat and release the fork
      availForks[indexFirstFork] = true;
      availForks[indexSecondFork] = true;

      pendingRequest.forEach((Request request) {
        if (availForks[request._firstFork] && availForks[request._secondFork]) {
          availForks[request._firstFork] = false;
          availForks[request._secondFork] = false;
          var id = request._id;
          SendPort reply = request._sendPort;

          //Log
          String s = "Forks for the philosopher ${philosophers[int.parse(id)].name} are available!";
          showLog ? _log.appendDebugMessage(s) : print(s);

          //Remove the current request and then reply forks are available
          new Future(() => pendingRequest.remove(request))
          ..then((_) => reply.send("ForkAvailable"));
        }
      });
    });
  }
}

/*
 * Accessories 
 */

class Philosopher {
  var _name, _state;


  Philosopher(this._name) {
    _state = "Think";
  }

  String get name => _name;
  String get state => _state;

  changeState(String state) {
    _state = state;
  }
}

class Request {
  var _sendPort, _id, _firstFork, _secondFork;

  Request(this._sendPort, this._id, this._firstFork, this._secondFork);
}

/*
 * Drawing Functions
 */
//Used in drawing
var rS = [65, 35];
var lR = [[0, 175], [62, 365], [271, 365], [336, 175], [165, 52]];
var lT = [[31, 200], [93, 390], [302, 390], [367, 200], [196, 78]];

initialize() {
  ctx2D.font = "20pt Calibri";
  ctx2D.textAlign = "center";
  drawBackground();
  //drawPhilosophersState();
}

redraw() {
  drawBackground();
  drawPhilosophersState();
}

drawBackground() {
  ctx2D.clearRect(0, 0, canvasElement.width, canvasElement.height);
  ctx2D.drawImageScaled(image, 0, 0, canvasElement.width, canvasElement.height);
}

drawPhilosophersState() {
  for (int i = 0; i < NPhilo; i++) {
    ctx2D.fillStyle = philosophers[i].state == "Think" ? "blue" : "red";
    ctx2D.fillRect(lR[i][0], lR[i][1], rS[0], rS[1]);
    ctx2D.fillStyle = "black";
    ctx2D.fillText(philosophers[i].state, lT[i][0], lT[i][1]);
  }
}
