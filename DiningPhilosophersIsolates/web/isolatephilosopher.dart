import 'dart:math';
import 'dart:isolate';

final rng = new Random();

var MyID;
var NPhilo;

var msgToSend = '';
Random randomGenerator;

var firstFork;
var secondFork;

ReceivePort myReceivePort;
SendPort waiterSendPort;

var state;

//Philosopher
main(List<String> args, SendPort sendPort) {
  /*
   * AN ISOLATE CANNOT CALL A PRINT!!
   * IT BLOCK WITHOUT SIGNAL THE ERROR IN DEBUG MODE
   */
  //print("Philooo");
  MyID = args[0];
  NPhilo = args[1];

  /*
   * ISOLATE ERRORS ARE NOT HANDLE VERY WELL
   * HERE, IF I USE secondFork = (firstFork+1)%NPhilo; IT STOPS WORKING
   * WITHOUT SIGNALING ANYTHING
   */

  //Forks assigned to this isolate
  firstFork = int.parse(MyID);
  secondFork = ((firstFork + 1) >= int.parse(NPhilo)) ? 0 : firstFork + 1;

  //Set the send and receive port
  waiterSendPort = sendPort;
  myReceivePort = new ReceivePort();

  //Update internal State
  state = "Initialize";

  //Set and send the message
  msgToSend = "I'm the Philosopher $MyID, using fork $firstFork and $secondFork, and there are $NPhilo philosophers";
  waiterSendPort.send(["Ready", msgToSend, myReceivePort.sendPort]);

  /*
  * WITH FUTURE IT DOESN'T WORK
  * BUT WITH SCHEDULE MICROTASK IT WORKS
  */
  //scheduleMicrotask(think);
  myReceivePort.listen(handleMessage);
}

handleMessage(var msg) {
  //Select the right msg handler
  if (msg == "Start") {
    think();
  } else if (msg == "ForkAvailable") {
    eat();
  } else if (msg == "Stop") {
    changeState();
  }
}

changeState() {
  if(state == "Think")
    wantsToEat();
  else if(state == "Eat")
    think();
}

think() {
  //Thinking
  state = "Think";
  //msgToSend = "Philosopher $MyID is thinking!";
  waiterSendPort.send([state, '$MyID', (rng.nextInt(20) + 1), myReceivePort.sendPort]);
  //new Future.delayed(new Duration(seconds:5),() => waiterSendPort.send(["Thinking",'$MyID']));
  //scheduleMicrotask(() => waiterSendPort.send(["Thinking",'$MyID']));
  /*
   * AN ISOLATE CANNOT CALL WAIT FUNCTION
   */
  //wait(5);
}

wantsToEat() {
  //Wants to Eat
  state = "WantsToEat";
  //msgToSend = "Philosopher $MyID wants to eat!";
  waiterSendPort.send([state, '$MyID', firstFork, secondFork, myReceivePort.sendPort]);

  //new Future.delayed(new Duration(seconds:randomGenerator.nextInt(5)), () => think());
}

eat() {
  //Eating
  state = "Eat";
  //msgToSend = "Philosopher $MyID is eating!";
  //new Timer(new Duration(seconds:1), () => {});
  waiterSendPort.send([state, '$MyID', (rng.nextInt(20) + 1), firstFork, secondFork, myReceivePort.sendPort]);
}

//THIS FUNCTION BLOCKS THE ISOLATE
//wait(num seconds) {
//  int milliSecToWait = new Random().nextInt(seconds*1000);
//  var currentMs = new DateTime.now().millisecondsSinceEpoch;
//  var endMs = currentMs * milliSecToWait;
//  while(currentMs < endMs) {
//    currentMs = new DateTime.now().millisecondsSinceEpoch;
//  }
//}
