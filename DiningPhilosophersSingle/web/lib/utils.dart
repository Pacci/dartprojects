part of diningPhilosophers;

class Request {
  Philosopher _phil;
  var _firstFork, _secondFork;

  Request(this._phil, this._firstFork, this._secondFork);

  Philosopher get phil => _phil;
  get first => _firstFork;
  get second => _secondFork;
  
  String toString() {
    return "${_phil.name} => fork$_firstFork ,fork$_secondFork";
  }
}

class Release {
  Philosopher _phil;
  var _firstFork, _secondFork;

  Release(this._phil, this._firstFork, this._secondFork);

  Philosopher get phil => _phil;
  get first => _firstFork;
  get second => _secondFork;
  
  String toString() {
    return "${_phil.name} => fork$_firstFork ,fork$_secondFork";
  }
}