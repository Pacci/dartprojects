part of diningPhilosophers;

//Philosopher class
class Philosopher {
  static int _duration = 20;
  static final _rng = new Random();

  int _id, _nTotPhilo, _firstFork, _secondFork;
  String _name, _currState;

  Philosopher(this._name, this._id, this._nTotPhilo) {
    _currState = 'Think';
    //Define forks
    {
      _firstFork = _id;
      _secondFork = ((_firstFork + 1) >= _nTotPhilo) ? 0 : _firstFork + 1;
    }
  }

  String get name => _name;
  String get currState => _currState;

  stopState() {
    _currState = 'Stop';
  }
  
  Future<Request> think() {
    _currState = 'Think';

    return new Future<Request>.delayed(new Duration(seconds: (_rng.nextInt(_duration) + 1)), () {
      Request r = new Request(this, _firstFork, _secondFork);
      return r;
    });
  }

  Future<Release> eat() {
    _currState = 'Eat';

    return new Future<Release>.delayed(new Duration(seconds: (_rng.nextInt(_duration) + 1)), () {
      Release r = new Release(this, _firstFork, _secondFork);
      return r;
    });
  }
}
