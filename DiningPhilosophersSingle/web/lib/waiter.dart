library diningPhilosophers;

import "dart:async";
import "dart:math";

part 'philosopher.dart';
part 'utils.dart';

//Waiter class
class Waiter {
  List<bool> _availForks;
  List<Request> _pendingRequest;

  Waiter() {
    _availForks = [true, true, true, true, true];
    _pendingRequest = new List<Request>();
  }

  Future<bool> handleRequest(Request r) {
    if(_pendingRequest.contains(r))
      _pendingRequest.remove(r);
    
    return new Future<bool>(() {
      if (_availForks[r.first] && _availForks[r.second]) {
        //Philo can eat!
        _availForks[r.first] = false;
        _availForks[r.second] = false;
        return true;
      } else {
        //Add to pending request
        _pendingRequest.add(r);
        return false;
      }
    });
  }
  
  Future<List<Request>> handleRelease(Release r) {
    _availForks[r.first] = true;
    _availForks[r.second] = true;
    
    return new Future<List<Request>>(() {
      List<Request> reqs = new List<Request>();    
      _pendingRequest.forEach((req) => reqs.add(req));
      
      return reqs;
    });
  }  
}