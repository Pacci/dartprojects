import 'dart:html';
import 'dart:async';
import 'lib/waiter.dart';
import 'lib/logUnit.dart';

final showLog = true;

ButtonElement startPhilo;
ButtonElement stopPhilo;

CanvasElement canvasElement;
CanvasRenderingContext2D ctx2D;
ImageElement image;

List<Philosopher> _philos;
Waiter _w;

bool _stopSimulation;
int _nFinished;
int _nFutures;

LogUnit _log;

main() {
  image = new ImageElement(src: "img/background.png");

  startPhilo = querySelector('#startDiningPhilosophers');
  stopPhilo = querySelector('#stopDiningPhilosophers');

  canvasElement = querySelector('#philoImage');
  ctx2D = canvasElement.context2D;

  startPhilo.onClick.listen((_) {
    startPhilo.disabled = true;
    stopPhilo.disabled = false;
    startPhilosophers();
  }); 

  stopPhilo.disabled = true;
  _stopSimulation = false;
  _nFinished = 0;
  stopPhilo.onClick.listen((_) {
    //Debug
    if (_log != null) _log.appendDebugMessage("Simulation stop!\nWait for the previous one to end..");
    startPhilo.disabled = true;
    stopPhilo.disabled = true;
    _stopSimulation = true;
  });

  image.onLoad.listen((_) => initialize());

  //Log
  if (showLog) _log = new LogUnit(querySelector('#debugText'));
}

stopSimulation() {
  print("Simulation has ended!");
  if (_nFutures == 0) {
    startPhilo.disabled = false;
    stopPhilo.disabled = true;
    _stopSimulation = false;

    //Debug
    if (_log != null) _log.appendDebugMessage("A new simulation can start!");
  }
}

startPhilosophers() {
  print("Start");

  _nFutures = 0;

  _w = new Waiter();
  _philos = [new Philosopher("Socrate", 0, 5), new Philosopher("Platone", 1, 5), new Philosopher("Talete", 2, 5), new Philosopher("Pitagora", 3, 5), new Philosopher("Eratostene", 4, 5)];

  //  PYRAMID OF DOOM
  //  _philos.forEach((philo) {
  //    philo.think()
  //    .then((request) {
  //      w.handleRequest(request)
  //      .then((canEat) {
  //        if(canEat)
  //          philo.changeState();
  //      });
  //    });
  //  });
  _philos.forEach((philo) => makePhiloThink(philo, _w));
}

//Spaghetti code
makePhiloThink(Philosopher phi, Waiter w) {
  if (!_stopSimulation) {
    _nFutures++;
    Future<Request> futureRequest = phi.think();

    futureRequest
        ..then((Request req) => tryToEat(w, req))
        ..whenComplete(() {
          print("A future has complete!\nFutures: ${--_nFutures}\n\n");
          if (_stopSimulation && _nFutures == 0) stopSimulation();
        });


    //Log
    if (_log != null) _log.appendDebugMessage("${phi.name} is thinking!");

    //Redraw the background
    /* scheduleMicrotask(redraw()) launches exceptions
     * scheduleMicrotask(redraw) does not
     */
  }
  else 
    phi.stopState();
  scheduleMicrotask(redraw);
}

tryToEat(Waiter w, Request req) {
  if (!_stopSimulation) {
    _nFutures++;
    Future<bool> futureRequestCanBeHandled = w.handleRequest(req);

    futureRequestCanBeHandled
        ..then((bool canEat) => makePhiloEatOrWait(req.phil, w, canEat))
        ..whenComplete(() {
          print("A future has complete!\nFutures: ${--_nFutures}\n\n");
          if (_stopSimulation && _nFutures == 0) stopSimulation();
        });

    //Log
    if (_log != null) _log.appendDebugMessage("${req.phil.name} wants to eat!");
  }
  else
    req.phil.stopState();
  scheduleMicrotask(redraw);
}

makePhiloEatOrWait(Philosopher phi, Waiter w, bool canEat) {
  if (!_stopSimulation) {
    if (canEat) {

      _nFutures++;
      Future<Release> futureRelease = phi.eat();

      futureRelease
          ..then((Release release) {

            _nFutures += 2;
            Future futurePhiloThink = new Future(() => makePhiloThink(phi, w));
            Future<List<Request>> futureHandleRelease = w.handleRelease(release);

            futurePhiloThink.whenComplete(() {
              print("A future has complete!\nFutures: ${--_nFutures}\n\n");
              if (_stopSimulation && _nFutures == 0) stopSimulation();
            });

            futureHandleRelease
                ..then((List<Request> list) {

                  _nFutures += list.length;
                  Future everyRequest = Future.forEach(list, (Request pReq) => tryToEat(w, pReq));
                  everyRequest.whenComplete(() {
                    print("A future has complete!\nFutures: ${_nFutures -= list.length}\n\n");
                    if (_stopSimulation && _nFutures == 0) stopSimulation();
                  });
                })
                ..whenComplete(() {
                  print("A future has complete!\nFutures: ${--_nFutures}\n\n");
                  if (_stopSimulation && _nFutures == 0) stopSimulation();
                });
          })
          ..whenComplete(() {
            print("A future has complete!\nFutures: ${--_nFutures}\n\n");
            if (_stopSimulation && _nFutures == 0) stopSimulation();
          });
      //Log
      if (_log != null) _log.appendDebugMessage("${phi.name} is eating!");
    }
  }
  else 
    phi.stopState();
  //Redraw the background
  scheduleMicrotask(redraw);
}

//Draw functions

var rS = [65, 35];
var lR = [[0, 175], [62, 365], [271, 365], [336, 175], [165, 52]];
var lT = [[31, 200], [93, 390], [302, 390], [367, 200], [196, 78]];

initialize() {
  ctx2D.font = "20pt Calibri";
  ctx2D.textAlign = "center";
  drawBackground();
}

redraw() {
  drawBackground();
  drawPhilosophersState();
}

drawBackground() {
  ctx2D.clearRect(0, 0, canvasElement.width, canvasElement.height);
  ctx2D.drawImageScaled(image, 0, 0, canvasElement.width, canvasElement.height);
}

drawPhilosophersState() {
  for (int i = 0; i < _philos.length; i++) {
    ctx2D.fillStyle = _philos[i].currState == "Think" ? "blue" : "red";
    ctx2D.fillRect(lR[i][0], lR[i][1], rS[0], rS[1]);
    ctx2D.fillStyle = "black";
    ctx2D.fillText(_philos[i].currState, lT[i][0], lT[i][1]);
  }
}
