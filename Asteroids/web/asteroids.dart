import 'dart:html';
import 'lib/asteroidgame.dart';

CanvasElement canvas;
CanvasRenderingContext2D ctx2D;

void main() {
  canvas = querySelector("#screen");
  ctx2D = canvas.context2D;

  AsteroidGame game = new AsteroidGame(canvas);
  game.start();

  canvas.onKeyUp.listen(game.handleKeyboardUp);
  canvas.onKeyDown.listen(game.handleKeyboardDown);
  canvas.onMouseMove.listen(game.handleMouseMove);
  canvas.onMouseDown.listen(game.handleMouseClick);
}