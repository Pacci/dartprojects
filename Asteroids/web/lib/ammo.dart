part of asteroidGame;

abstract class Ammo {
  String _type;
  int _damage;

  num _posX, _posY;
  
  num _speedDirX, _speedDirY;
  num _magn;

  //Get methods
  String get type => _type;
  
  int get damage => _damage;
  
  num get posX => _posX;
  num get posY => _posY;

  num get speedDirX => _speedDirX;
  num get speedDirY => _speedDirY;
  num get magnitude => _magn;

  //Utils functions (the move function is the same for every bullet that moves in a straight line)
  move(num deltaT) {
    _posX += (_speedDirX * _magn) * deltaT;
    _posY += (_speedDirY * _magn) * deltaT;
  }

  bool collide(Asteroid asteroid);
  
  bool outOfBounds(num originX, num originY, num width, num height);

  drawAmmo(CanvasRenderingContext2D ctx2D);
}

class Bullet extends Ammo {
  static final _bulletDamage = 1;
  static final _boundingBox = 2;

  //Cannot assign usign this._posX from the super class Ammo
  Bullet(num posX, num posY, num speedX, num speedY) {
    _type = "Bullet";
    _posX = posX;
    _posY = posY;
    _speedDirX = speedX;
    _speedDirY = speedY;

    _magn = 5;
    _damage = _bulletDamage;
  }
  
  //Utils functions
  bool collide(Asteroid asteroid) {
    num disX = asteroid.posX - _posX;
    num disY = asteroid.posY - _posY;
    num distance = sqrt(pow(disX, 2) + pow(disY, 2));
    
    if(distance < _boundingBox+(asteroid.sizeX/2))
        return true;
    else
      return false;
  }
  
  bool outOfBounds(num originX, num originY, num width, num height) {
    if (_posX + _boundingBox < originX || _posX - _boundingBox > width || _posY + _boundingBox < originY || _posY - _boundingBox > height) 
      return true; 
    else 
      return false;
  }
  
  //Draw function
  drawAmmo(CanvasRenderingContext2D ctx2D) {
    ctx2D
        ..strokeStyle = "red"
        ..beginPath()
        ..arc(_posX, _posY, _boundingBox, 0, 2 * PI)
        ..stroke();
  }
}

class Rocket extends Ammo {
  static final _rocketDamage = 5;
  static final _boundingBox = 5;

  //Cannot assign usign this._posX from the super class Ammo
  Rocket(num posX, num posY, num speedX, num speedY) {
    _type = "Rocket";
    _posX = posX;
    _posY = posY;
    _speedDirX = speedX;
    _speedDirY = speedY;

    _magn = 10;
    _damage = _rocketDamage;
  }
  
  //Utils functions
  bool collide(Asteroid asteroid) {
    num disX = asteroid.posX - _posX;
    num disY = asteroid.posY - _posY;
    num distance = sqrt(pow(disX, 2) + pow(disY, 2));
    
    if(distance < _boundingBox+(asteroid.sizeX/2) || 
        distance < _boundingBox+(asteroid.sizeY/2))
        return true;
    else
      return false;
  }
  
  bool outOfBounds(num originX, num originY, num width, num height) {
    if (_posX + _boundingBox < originX || _posX - _boundingBox > width || _posY + _boundingBox < originY || _posY - _boundingBox > height) 
      return true; 
    else 
      return false;
  }
  
  //Draw function
  drawAmmo(CanvasRenderingContext2D ctx2D) {
    ctx2D
        ..fillStyle = "red"
        ..beginPath()
        ..arc(_posX, _posY, _boundingBox, 0, 2 * PI)
        ..fill();
  }
}