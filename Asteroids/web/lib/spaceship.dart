part of asteroidGame;

class SpaceShip {
  static final int _defaultNBullets = 500;
  static final int _defaultNRockets = 20;
  static final num _defaultAngle = 0;
  static final _maxSpeed = 50;
  static final _dim = 10;

  num _posX, _posY;
  num _angle;
  num _speedX, _speedY;

  //Map type of bullet-number of remaining bullets of that type
  List _munitions;
  int _index;

  SpaceShip(this._posX, this._posY, {num angle, num speedX, num speedY}) {
    if (angle == null) _angle = _defaultAngle; else _angle = angle;
    if (speedX == null) _speedX = 0; else _speedX = speedX;
    if (speedY == null) _speedY = 0; else _speedY = speedY;

    _munitions = [[Bullet, _defaultNBullets], [Rocket, _defaultNRockets]];
    _index = 0;
  }

  //Get methods
  num get posX => _posX;
  num get posY => _posY;
  
  num get angle => _angle;
  
  num get speedX => _speedX;
  num get speedY => _speedY;

  //Utils functions
  List getMunitionState() {
    return _munitions;
  }
  
  changeWeapon() {
    _index++;
    if (_index >= _munitions.length) _index = 0;
  }

  Ammo getAmmo() {
    //if current munitions are finished, return null
    if(_munitions[_index][1] <= 0) {
      _munitions[_index][1] = 0;
      return null;
    }
    else {
      //px and py point position where the bullet starts
      num cosAngle = double.parse(cos(_angle).toStringAsPrecision(3));
      num sinAngle = double.parse(sin(_angle).toStringAsPrecision(3));
      num px = ((_dim / 2) * sinAngle) + _posX;
      num py = ((-_dim / 2) * cosAngle) + _posY;

      //dimx and dimy unit vector direction of the bullet
      num dimx = px - _posX;
      num dimy = py - _posY;
      num magn = sqrt(pow(dimx, 2) + pow(dimy, 2));
      dimx = dimx / magn;
      dimy = dimy / magn;

      _munitions[_index][1]--;

      if (_munitions[_index][0] == Bullet) 
        return new Bullet(px, py, dimx, dimy); 
      else //if (_munitions[index][0] == Rocket)
        return new Rocket(px, py, dimx, dimy);
    }
  }

  speedUp(num deltaVX, num deltaVY) {
    _speedX += deltaVX;
    _speedY += deltaVY;
    
    if(_speedX.abs() > _maxSpeed) {
      _speedX = (_speedX<0) ? -_maxSpeed : _maxSpeed;
    }
    if(_speedY.abs() > _maxSpeed) {
      _speedY = (_speedY<0) ? -_maxSpeed : _maxSpeed;
    }
  }

  move(num deltaT) {
    _posX += _speedX * deltaT;
    _posY += _speedY * deltaT;
  }

  changeAngle(num pointX, num pointY) {
    num difX = pointX - _posX;
    num difY = pointY - _posY;

    _angle = atan(difY / difX);

    if (difX < 0) {
      _angle -= PI / 2;
    } else {
      _angle += PI / 2;
    }
  }

  bool collide(Asteroid asteroid) {
    num disX = asteroid.posX - _posX;
    num disY = asteroid.posY - _posY;
    num distance = sqrt(pow(disX, 2) + pow(disY, 2));
    
    if(distance < (_dim / 2)+(asteroid.sizeX/2))
        return true;
    else
      return false;
  }  
  
  bool outOfBounds(num originX, num originY, num width, num height) {
    if (_posX + (_dim / 2) < originX || _posX - (_dim / 2) > width || _posY + (_dim / 2) < originY || _posY - (_dim / 2) > height) 
      return true; 
    else 
      return false;
  }

  
  //Draw function
  drawSpaceShip(CanvasRenderingContext2D ctx2D) {
    num cosAngle = double.parse(cos(_angle).toStringAsPrecision(3));
    num sinAngle = double.parse(sin(_angle).toStringAsPrecision(3));

    num p1x = ((_dim / 2) * sinAngle) + _posX;
    num p1y = ((-_dim / 2) * cosAngle) + _posY;

    num p2x = ((_dim / 2) * cosAngle - (_dim / 2) * sinAngle) + _posX;
    num p2y = ((_dim / 2) * sinAngle + (_dim / 2) * cosAngle) + _posY;

    num p3x = ((-_dim / 2) * cosAngle - (_dim / 2) * sinAngle) + _posX;
    num p3y = ((-_dim / 2) * sinAngle + (_dim / 2) * cosAngle) + _posY;

    ctx2D
        ..strokeStyle = "white"
        ..beginPath()
        ..moveTo(p1x, p1y)
        ..lineTo(p2x, p2y)
        ..lineTo(_posX, _posY)
        ..lineTo(p3x, p3y)
        ..lineTo(p1x, p1y)
        ..stroke();
  }
}