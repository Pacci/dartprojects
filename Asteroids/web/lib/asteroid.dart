part of asteroidGame;

class Asteroid {
  static final int _defaultSizeX = 20;
  static final int _defaultSizeY = 20;
  static final int _defaultHealth = 10;
  static final int _defaultMagn = 2;

  //Position (could be used a class Point)
  num _posX, _posY;
  //Direction and speed (could be used a class Vector)
  num _speedDirX, _speedDirY;
  num _magn;

  //Size of the asteroids (only square now)
  int _sizeX, _sizeY;
  //Health
  int _health;

  Asteroid(this._posX, this._posY, this._speedDirX, this._speedDirY, {num magn, num sizeX, num sizeY, num hp}) {
    if(magn == null) _magn = _defaultMagn; else _magn = magn;
    if(sizeX == null) _sizeX = _defaultSizeX; else _sizeX = sizeX;
    if(sizeY == null) _sizeY = _defaultSizeY; else _sizeY = sizeY;
    if(hp == null) _health = _defaultHealth; else _health = hp;
  }

  //Get methods
  num get posX => _posX;
  num get posY => _posY;
  
  num get speedDirX => _speedDirX;
  num get speedDirY => _speedDirY;
  num get magnitude => _magn;
  
  num get sizeX => _sizeX;
  num get sizeY => _sizeY;
  
  num get health => _health;
  
  //Utils functions
  bool takeDamage(int damage) {
    return (_health-=damage) <= 0;
  }
  
  move(num deltaT) {
    _posX += (_speedDirX * _magn) * deltaT;
    _posY += (_speedDirY * _magn) * deltaT;
  }

  bool outOfBounds(num originX, num originY, num width, num height) {
    if (_posX + (_sizeX / 2) < originX || _posX - (_sizeX / 2) > width || _posY + (_sizeY / 2) < originY || _posY - (_sizeY / 2) > height) return true; else return false;
  }

  //Draw functions
  drawAsteroid(CanvasRenderingContext2D ctx2D) {
    num halfSizeX = _sizeX / 2;
    num halfSizeY = _sizeY / 2;
    ctx2D
        ..strokeStyle = "white"
        ..beginPath()
        ..strokeRect(_posX - halfSizeX, _posY - halfSizeY, _sizeX, _sizeY);
    
    drawHp(ctx2D, _posX, _posY);
  }
  
  drawHp(CanvasRenderingContext2D ctx2D, num midX, num midY) {
    ctx2D
    ..font = "10pt Calibri"
    ..textAlign = "center"
    ..strokeStyle = "lightgreen"
    ..strokeText("${_health}", midX, midY+2);
  }
}