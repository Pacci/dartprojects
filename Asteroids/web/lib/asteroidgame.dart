library asteroidGame;

import 'dart:html';
import 'dart:math';
import 'dart:async';

part 'spaceship.dart';
part 'ammo.dart';
part 'asteroid.dart';

class AsteroidGame {
  //Variables for generating asteroids
  static final Random _asteroidGen = new Random();
  num _slowChanging;
  
  //Display
  CanvasElement _canvas;
  num _spaceSizeX, _spaceSizeY;
  
  //Elements in the game
  SpaceShip _player;
  List<Asteroid> _asteroids;
  List<Ammo> _ammos;

  //State and score
  String _state;
  int _score;
  
  //Input variables
  Map<int, int> keyMap = {};
  bool _shoot = false;
  num _posXmouse = 0, _posYmouse = 0;

  AsteroidGame(this._canvas) {
    _spaceSizeX = _canvas.width;
    _spaceSizeY = _canvas.height;
  }

  //keyboard events
  handleKeyboardDown(KeyboardEvent e) {
    keyMap[new KeyEvent.wrap(e).keyCode] = e.timeStamp;
  }

  handleKeyboardUp(KeyboardEvent e) {
    keyMap.remove(new KeyEvent.wrap(e).keyCode);
  }

  handlingKeyboard() {
    if (_state == "Running") {
      if (keyMap.containsKey(KeyCode.W)) {
        //UP
        _player.speedUp(0, -2);
      }
      if (keyMap.containsKey(KeyCode.S)) {
        //DOWN
        _player.speedUp(0, 2);
      }
      if (keyMap.containsKey(KeyCode.A)) {
        //LEFT
        _player.speedUp(-2, 0);
      }
      if (keyMap.containsKey(KeyCode.D)) {
        //RIGHT
        _player.speedUp(2, 0);
      }
      if (keyMap.containsKey(KeyCode.E)) {
        //Always remove
        _player.changeWeapon();
        keyMap.remove(KeyCode.E);
      }
    } else if (_state == "End") {
      if (keyMap.containsKey(KeyCode.R)) {
        //Always remove
        keyMap.remove(KeyCode.R);
        init();
      }
    }
  }
  
  //Mouse events
  handleMouseMove(MouseEvent e) {
    Point pg = e.page;
    Rectangle rec = _canvas.getBoundingClientRect();

    _player.changeAngle(pg.x - rec.left, pg.y - rec.top);
  }

  handleMouseClick(MouseEvent e) {
    if (_state == "Init") {
      _state = "Running";
    } else if (_state == "Running") {
      _shoot = true;
    }
  }

  //Game loop
  start() {
    init();
    window.animationFrame.then(gameLoop);
  }
  
  init() {
    _player = new SpaceShip(_spaceSizeX / 2, _spaceSizeY / 2);
    _asteroids = new List<Asteroid>();
    _ammos = new List<Ammo>();
    
    _state = "Init";
    _score = 0;
    _slowChanging = 0.0;
  }

  gameLoop(num delta) {
    if (_state == "Init") {
      startScreen();
      window.animationFrame.then(gameLoop);
    } 
    else if (_state == "Running") {
      handlingAsteroids();
      handlingKeyboard();
      updateState();
      drawImage();
      
      //Display the score
      drawText("Score: $_score",25,_canvas.height-5,10);
      //Display the ammos
      int pos = 200;
      for(var bullets in _player.getMunitionState()) {
        drawText("${bullets[0]}: ${bullets[1]}",pos,_canvas.height-5,10);
        pos += 75;
      }
      window.animationFrame.then(gameLoop);
    } 
    else if (_state == "End") {
      handlingKeyboard();
      endScreen();
      window.animationFrame.then(gameLoop);
    }
  }
  
  startScreen() {
    drawImage();
    drawText("CLICK", _spaceSizeX / 2, _spaceSizeY / 2, 40);
    drawText("WASD to move", _spaceSizeX / 2, _spaceSizeY / 2+80, 15);
    drawText("E to change weapon", _spaceSizeX / 2, _spaceSizeY / 2+100, 15);
  }
  
  endScreen() {
    drawBackground();
    drawText("GAME OVER", _spaceSizeX / 2, _spaceSizeY / 2, 60);
    drawText("Final Score: $_score",_spaceSizeX / 2, _spaceSizeY / 2 + 40,30);
    drawText("Press R to restart!", _spaceSizeX / 2, _spaceSizeY / 2 + 70, 20);
  }

  handlingAsteroids() {
    if (_asteroidGen.nextInt(100) < (2+_slowChanging)) {
      //GENERATE
      _slowChanging += 0.1;
      generateAsteroid().then((asteroid) {
        if (_state == "Running") _asteroids.add(asteroid);
      });
    }
  }

  Future<Asteroid> generateAsteroid() {
    //Random select from wich side appear, 0=top; 1=right; 2=bot; 3=left;
    int side = _asteroidGen.nextInt(4);

    Point startingPoint;
    //Random select a point from the chosen side
    if (side == 0) 
      startingPoint = new Point(_asteroidGen.nextInt(_canvas.width), 0); 
    else if (side == 2) startingPoint = new Point(_asteroidGen.nextInt(_canvas.width), _canvas.height); 
    else if (side == 1) startingPoint = new Point(_canvas.width, _asteroidGen.nextInt(_canvas.height)); 
    else //if(side == 3)
    startingPoint = new Point(0, _asteroidGen.nextInt(_canvas.height));

    //Direction the ship (plus could be added a variance)
    num dimx = _player.posX - startingPoint.x;
    num dimy = _player.posY - startingPoint.y;
    num magn = sqrt(pow(dimx, 2) + pow(dimy, 2));
    dimx = dimx / magn;
    dimy = dimy / magn;

    return new Future.delayed(new Duration(seconds: _asteroidGen.nextInt(10)), () => new Asteroid(startingPoint.x, startingPoint.y, dimx, dimy));
  }

  updateState() {
    handlingKeyboard();
    //Handling Astroship
    _player.move(0.04);
    if (_player.outOfBounds(0, 0, _canvas.width, _canvas.height)) new Future(() => _state = "End");
    //Handling bullets
    for (Ammo am in _ammos) {
      am.move(0.5);
      if (am.outOfBounds(0, 0, _canvas.width, _canvas.height)) new Future(() => _ammos.remove(am));
      else {
        for (Asteroid as in _asteroids) {
          if(am.collide(as)) {
            new Future(() => _ammos.remove(am));
            _score += am.damage;
            if(as.takeDamage(am.damage)) {
              _asteroids.remove(as);
              _score += 10;
            }
            break;
          }
        }
      }
    }
    if (_shoot) {
      Ammo bullet = _player.getAmmo();
      if(bullet != null)
        _ammos.add(bullet);
    }
    _shoot = false;

    for (Asteroid as in _asteroids) {
      as.move(0.5);
      if (as.outOfBounds(0, 0, _canvas.width, _canvas.height)) new Future(() => _asteroids.remove(as));
      else if(_player.collide(as))
        new Future(() => _state = "End");
    }
  }

  //Draw functions
  drawImage() {
    drawBackground();

    _player.drawSpaceShip(_canvas.context2D);
    for (Ammo am in _ammos) am.drawAmmo(_canvas.context2D);
    for (Asteroid as in _asteroids) as.drawAsteroid(_canvas.context2D);
  }

  drawBackground() {
    _canvas.context2D.fillStyle = "black";
    _canvas.context2D.fillRect(0, 0, _canvas.width, _canvas.height);
  }

  drawText(String text, num posX, num posY, num size) {
    _canvas.context2D
        ..font = "${size}pt Calibri"
        ..textAlign = "center"
        ..strokeStyle = "white"
        ..strokeText(text, posX, posY);
  }
}