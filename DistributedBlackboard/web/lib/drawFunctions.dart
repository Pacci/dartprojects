library drawFunctions;

import 'dart:html';
import 'dart:math';

//These functions are used from the user to display the state.
//Keep in mind that adding a new figure means drawFigure has to be updated

//Draw the pointer of the user(s)
drawCrossPointer(CanvasRenderingContext2D ctx2D, Point p, {String fillStyle, String strokeStyle, int size}) {
  String actualFillStyle = fillStyle == null ? 'black' : fillStyle;
  String actualStrokeStyle = strokeStyle == null ? 'black' : strokeStyle;
  int actualSize = size == null ? 8 : size;
  if(actualSize < 8) actualSize = 8;
  
  //Draw the pointer
  ctx2D
      ..fillStyle = actualFillStyle
      ..strokeStyle = actualStrokeStyle
      ..beginPath()
      ..moveTo(p.x,p.y-actualSize)
      ..lineTo(p.x,p.y+actualSize)
      ..moveTo(p.x-actualSize,p.y)
      ..lineTo(p.x+actualSize,p.y)
      ..fill()
      ..stroke()
      ..closePath();
}

//Drat the figure(s)
drawFigure(CanvasRenderingContext2D ctx2D, Map figure) {
  Map rgbaChannel = figure['rgbaChan'];
  
  if(figure['figure'] == 'rectangle') {
    //Get the values
    num initPointX = figure['initialPointX'];
    num initPointY = figure['initialPointY'];
    num width = figure['width'];
    num height = figure['height'];
    
    //Draw the rectangle
    ctx2D
        ..setFillColorRgb(rgbaChannel['r'], rgbaChannel['g'], rgbaChannel['b'], rgbaChannel['a'])
        //..setStrokeColorRgb(f.rgbaChan['r'], f.rgbaChan['g'], f.rgbaChan['b'], f.rgbaChan['a'])
        ..strokeStyle = 'black'
        ..beginPath()
        ..fillRect(initPointX, initPointY, width, height)
        ..strokeRect(initPointX, initPointY, width, height);
  }
  else if(figure['figure'] == 'ellipse') {
    //Get the values
    num initPointX = figure['initialPointX'];
    num initPointY = figure['initialPointY'];
    num diamX = figure['diamX'];
    num diamY = figure['diamY'];
    //Calculate center and radius from previous values
    num centerX = initPointX + (diamX / 2);
    num centerY = initPointY + (diamY / 2);
    num radiusX = (diamX / 2).abs();
    num radiusY = (diamY / 2).abs();
    
    //Draw the ellipse
//    ctx2D
//        ..setFillColorRgb(rgbaChannel['r'], rgbaChannel['g'], rgbaChannel['b'], rgbaChannel['a'])
//        //..setStrokeColorRgb(f.rgbaChan['r'], f.rgbaChan['g'], f.rgbaChan['b'], f.rgbaChan['a'])
//        ..strokeStyle = 'black'
//        ..beginPath()
//        ..ellipse(centerX, centerY, radiusX, radiusY, 0, 0, 2 * PI, false)
//        ..stroke()
//        ..fill();
    
    //The funcion ellipse works only in chrome (and chromium).
    //It needs an additional function that use arc to draw an ellipse if the client use other browsers
    ctx2D
        ..setFillColorRgb(rgbaChannel['r'], rgbaChannel['g'], rgbaChannel['b'], rgbaChannel['a'])
        //..setStrokeColorRgb(f.rgbaChan['r'], f.rgbaChan['g'], f.rgbaChan['b'], f.rgbaChan['a'])
        ..strokeStyle = 'black'
        ..save()
        ..beginPath()
        ..translate(centerX-radiusX, centerY-radiusY)
        ..scale(radiusX, radiusY)
        ..arc(1, 1, 1, 0, 2 * PI, false)
        ..restore()
        ..stroke()
        ..fill();  
  }
  else if(figure['figure'] == 'line') {
    //Get the values
    num startPointX = figure['startPointX'];
    num startPointY = figure['startPointY'];
    num endPointX = figure['endPointX'];
    num endPointY = figure['endPointY'];
    
    //Draw the line
    ctx2D
        ..setStrokeColorRgb(rgbaChannel['r'], rgbaChannel['g'], rgbaChannel['b'], rgbaChannel['a'])
        //..setStrokeColorRgb(f.rgbaChan['r'], f.rgbaChan['g'], f.rgbaChan['b'], f.rgbaChan['a'])
        //..strokeStyle = 'black'
        ..beginPath()
        ..moveTo(startPointX, startPointY)
        ..lineTo(endPointX, endPointY)
        ..stroke();
  }
}