library blackboard;

import "dart:math";
import "dart:convert";
import "package:uuid/uuid.dart";

part 'user.dart';
part 'figure.dart';

class Blackboard {
  //ID GENERATOR
  static final _uuid = new UuidBase();
  //Sizes of the blackboard
  num _bbSizeX, _bbSizeY;
  //Max number of users allowed
  int _maxUsers;

  //Map of users connected, id->user
  Map<String, User> _userMap;

  //Options available with descriptions
  //All users with the same options available
  final Map _optionsAvailable = {
    "select": {"subOptions": [{"name":"dragndrop", "description":"Drag and drop a figure"}]},
    "draw": {"subOptions": [{"name":"rect", "description":"Draw a rectangle"},
                            {"name":"ellipse", "description":"Draw an ellipse"},
                            {"name":"line", "description":"Draw a line"}]},
    "delete":  {"subOptions": [{"name":"selected", "description":"Delete a single figure"},
                               {"name":"all", "description":"Delete all the figures"}]},
  };

  //Two different areas, one for the figures in drawing/modifying state, the other for figures already drawn
  Map<String, ColoredFigure> _stagingArea;
  List<ColoredFigure> _figureDrawn;

  //Used for composed distributed resize
  Map<ColoredFigure, Map<String, Point>> _reDrawDictionary;

  Blackboard(this._bbSizeX, this._bbSizeY, this._maxUsers) {
    _userMap = new Map<String, User>();

    _stagingArea = new Map<String, ColoredFigure>();
    _figureDrawn = new List<ColoredFigure>();
    _reDrawDictionary = new Map<ColoredFigure, Map<String, Point>>();
  }

  //Return the id of the user
  String connectUser({String name, int privilegeLevel}) {
    if (_userMap.length < _maxUsers) {
      //A username that could be used for chat sessions, for now is simply used as an alias to hide the actual id
      String actualName = (name != null) ? name : "User${_userMap.length}";
      //This could be used to change what kind of operations can do a certain user!
      int actualPrivLevel = (privilegeLevel != null) ? privilegeLevel : 3;

      String id = _uuid.v1();
      _userMap[id] = new User(actualName, _optionsAvailable);

      return id;
    } else 
      return null;
  }

  /*
   * Check first if the user id is present in this blackboard, then will pass the arguments to the
   * user trying to change its state (if the option/suboption are available to that user).
   */
  changeUserState(String id, String option, String subOption) {
    if (_userMap.keys.contains(id)) _userMap[id].changeState(option, subOption);
  }

  /*
   * Get user by the id
   */
  User getById(String id) {
    return _userMap[id];
  }
  /*
   * Try to remove a user passing the id. If present, it will remove and return true, otherwise 
   * will return false. 
   */
  bool removeUser(String id) {
    if (_userMap.remove(id) != null) 
      return true; 
    else 
      return false;
  }

  /*
   * List of events that the blackboard can handle
   */
  mouseDownEvent(String id, Point p) {
    //Check user id
    if (_userMap.containsKey(id)) {

      //Update the pointer position
      _userMap[id].pointerPosition = p;

      //Check if he is not already doing something (in staging area)
      if (!_stagingArea.containsKey(id)) {
        if (_userMap[id].optSel == "draw") {
          if (_userMap[id].subOptSel == 'rect') 
            _stagingArea[id] = new RectangleFigure(p.x, p.y, 0, 0).setRGBAChannel(a: 0.5); 
          else if (_userMap[id].subOptSel == 'ellipse') 
            _stagingArea[id] = new EllipseFigure(p.x, p.y, 0, 0).setRGBAChannel(a: 0.5);
          else if (_userMap[id].subOptSel == 'line') 
            _stagingArea[id] = new LineFigure(p.x, p.y, p.x, p.y).setRGBAChannel(a: 0.5);
        }

        if (_userMap[id].optSel == "select") {
          if (_userMap[id].subOptSel == 'dragndrop') {
            ColoredFigure f = pointInsideAFigure(p);
            if (f != null) {
              //Remove element from drawn
              if (_figureDrawn.remove(f)) {
                //Add element in reDrawDictionary
                _reDrawDictionary[f] = new Map<String, Point>();
                _reDrawDictionary[f][id] = p;
                //Add element in the staging area to modify
                _stagingArea[id] = f.setRGBAChannel(a: 0.5);
              }
            }
          }
        }

        if (_userMap[id].optSel == "delete") {
          if (_userMap[id].subOptSel == 'selected') {
            ColoredFigure f = pointInsideAFigure(p);
            if (f != null) {
              _figureDrawn.remove(f);
            }
          }
          else if (_userMap[id].subOptSel == 'all') {
            _stagingArea.clear();
            _figureDrawn.clear();
          }
        }
      }
    }
  }

  mouseMoveEvent(String id, Point p) {
    //Check user id
    if (_userMap.containsKey(id)) {

      //Update the pointer position
      _userMap[id].pointerPosition = p;

      //Check if he is doing something (in staging area)
      if (_stagingArea.containsKey(id)) {
        if (_userMap[id].optSel == "draw") {
          ColoredFigure f = _stagingArea[id];
          if (f is RectangleFigure) {
            f.width = p.x - f.left;
            f.height = p.y - f.top;
          } else if (f is EllipseFigure) {
            f.diamX = p.x - f.left;
            f.diamY = p.y - f.top;
          } else if (f is LineFigure) {
            f.endX = p.x;
            f.endY = p.y;
          }
        }

        if (_userMap[id].optSel == "select") {
          ColoredFigure f = _stagingArea[id];
          //Get the previous point
          Point oldP = _reDrawDictionary[f][id];
          if (f is RectangleFigure) {
            f.left += (p.x - oldP.x);
            f.top += (p.y - oldP.y);
          } else if (f is EllipseFigure) {
            f.left += (p.x - oldP.x);
            f.top += (p.y - oldP.y);
          } else if (f is LineFigure) {
            num difX = (p.x - oldP.x);
            num difY = (p.y - oldP.y);
            f.startX += difX;
            f.startY += difY;
            f.endX += difX;
            f.endY += difY;
          }
          //Set the new point
          _reDrawDictionary[f][id] = p;
        }
      }
    }
  }

  mouseUpEvent(String id, Point p) {
    //Check user id
    if (_userMap.containsKey(id)) {

      //Update the pointer position
      _userMap[id].pointerPosition = p;

      //Check if he is doing something (in staging area)
      if (_stagingArea.containsKey(id)) {
        if (_userMap[id].optSel == "draw" || _userMap[id].optSel == "select") {
          //remove element from staging area and from reDrawDictionary
          ColoredFigure f = _stagingArea.remove(id);
          _reDrawDictionary.remove(f);
          _figureDrawn.add(f.setRGBAChannel(a: 1));
        }
      }
    }
  }
  //Utilities
  ColoredFigure pointInsideAFigure(Point p) {
    //Start to search from the last element of the list, in this way will be selected always the figure on top
    for(int i = _figureDrawn.length-1; i>=0; i--) {
      if (_figureDrawn[i].checkPointInside(p)) {
        return _figureDrawn[i];
      }
    }
    return null;
  }

  Map stateAsJson() {
    Map _userJsonMap = new Map();
    List _figureDrawnJsonList = new List();
    Map _stagingJsonMap = new Map();

    for (String id in _userMap.keys) _userJsonMap[_userMap[id].username] = {
      'pointerPositionX': _userMap[id].pointerPosition.x,
      'pointerPositionY': _userMap[id].pointerPosition.y
    };

    for (ColoredFigure f in _figureDrawn) 
      _figureDrawnJsonList.add(f.toJson());

    for (String id in _stagingArea.keys) 
      _stagingJsonMap[_userMap[id].username] = _stagingArea[id].toJson();

    return {
      'users': _userJsonMap,
      'drawn': _figureDrawnJsonList,
      'staging': _stagingJsonMap,
    };
  }

  String stateToString() {
    String result = "BlackBoardState:";
    //Users connected and state
    result += "\n[Users] \nUser:";
    for (User u in _userMap.values) result += "${u.toString()} ";

    //Figure on the blackboard
    //Drawn
    result += "\n[Figures] \nDrawn:";
    for (ColoredFigure f in _figureDrawn) result += "${f.toString()} ";

    //Staging area
    result += "\n\nStagingArea: ";

    for (ColoredFigure f in _stagingArea.values) result += "${f.toString()} ";

    //Redraw area
    result += "\n\nRedrawArea: ";

    for (ColoredFigure f in _reDrawDictionary.keys) result += "${f.toString()} ";

    return result;
  }
}