part of blackboard;

abstract class ColoredFigure {
  //Color figure RGB + alpha-channel
  Map _rgbaChan = {
    "r": 0,
    "g": 0,
    "b": 0,
    "a": 1
  };

  setRGBAChannel({int r, int g, int b, num a}) {
    if (r != null) {
      if (r > 255) _rgbaChan['r'] = 255; else if (r < 0) _rgbaChan['r'] = 0; else _rgbaChan['r'] = r;
    }
    if (g != null) {
      if (g > 255) _rgbaChan['g'] = 255; else if (g < 0) _rgbaChan['g'] = 0; else _rgbaChan['g'] = g;
    }
    if (b != null) {
      if (b > 255) _rgbaChan['b'] = 255; else if (b < 0) _rgbaChan['b'] = 0; else _rgbaChan['b'] = b;
    }
    if (a != null) {
      if (a > 1) _rgbaChan['a'] = 1; else if (a < 0) _rgbaChan['a'] = 0; else _rgbaChan['a'] = a;
    }
    return this;
  }

  bool checkPointInside(Point p);
  Map toJson();
}

class LineFigure extends ColoredFigure {
  static final int _defaultR = 0;
  static final int _defaultG = 0;
  static final int _defaultB = 255;
  static final num _defaultA = 1.0;
  
  num _startX, _startY, _endX, _endY;
  
  LineFigure(this._startX, this._startY, this._endX, this._endY, {int r, int g, int b, num a}) {
    int tempR = (r == null) ? _defaultR : r;
    int tempG = (g == null) ? _defaultG : g;
    int tempB = (b == null) ? _defaultB : b;
    num tempA = (a == null) ? _defaultA : a;
    setRGBAChannel(r: tempR, g: tempG, b: tempB, a: tempA);
  }
  
  num get startX => _startX;
  set startX(num value) => _startX = value;
  num get startY => _startY;
  set startY(num value) => _startY = value;
  
  num get endX => _endX;
  set endX(num value) => _endX = value;
  num get endY => _endY;
  set endY(num value) => _endY = value;
  
  bool checkPointInside(Point p) {
    //Check if the cross product of (b-a) and (c-a) is 0, tells you if the points a, b and c are aligned.
    //Check that the dot product of (b-a) and (c-a) is positive and is less than 
    //the square of the distance between a and b, tells you if the points c is between a and b.
    num epsilon = 5;
    
    num a = (_endY - _startY) / (_endX - _startX);
    num b = _startY - a * _startX;
    if ( (p.y - (a*p.x+b)).abs() < epsilon)
    {
        return true;
    }
    return false;
  }

  Map toJson() {
    return {
      'figure': 'line',
      'startPointX': _startX,
      'startPointY': _startY,
      'endPointX': _endX,
      'endPointY': _endY,
      'rgbaChan': _rgbaChan
    };
  }

  String toString() {
    return "Line[StartPoint(${_startX},${_startY}), EndPoint(${_endX},${_endY})]";
  }
}

class RectangleFigure extends ColoredFigure {
  static final int _defaultR = 255;
  static final int _defaultG = 0;
  static final int _defaultB = 0;
  static final num _defaultA = 1.0;

  num _left, _top;
  num _width, _height;

  RectangleFigure(this._left, this._top, this._width, this._height, {int r, int g, int b, num a}) {
    int tempR = (r == null) ? _defaultR : r;
    int tempG = (g == null) ? _defaultG : g;
    int tempB = (b == null) ? _defaultB : b;
    num tempA = (a == null) ? _defaultA : a;
    setRGBAChannel(r: tempR, g: tempG, b: tempB, a: tempA);
  }

  RectangleFigure.fromJson(String jsonString) {
    Map data = JSON.decode(jsonString);
    _left = data['initialPointX'];
    _top = data['initialPointY'];
    _width = data['width'];
    _height = data['height'];
    _rgbaChan = JSON.decode(data['rgbaChan']);
  }

  num get left => _left;
  set left(num value) => _left = value;
  num get top => _top;
  set top(num value) => _top = value;

  num get width => _width;
  set width(num value) => _width = value;
  num get height => _height;
  set height(num value) => _height = value;

  num get right => _left + _width;
  set right(num value) => _left = value - _width;
  num get bottom => _top + _height;
  set bottom(num value) => _top = value - _height;

  bool checkPointInside(Point p) {
    //Width and height can be negative so we have to check first
    bool cond1 = (width >= 0);
    bool cond2 = (height >= 0);

    bool check1 = cond1 ? (p.x >= _left && p.x <= this.right) : (p.x <= _left && p.x >= this.right);
    bool check2 = cond2 ? (p.y >= _top && p.y <= this.bottom) : (p.y <= _top && p.y >= this.bottom);
    if (check1 && check2) return true; else return false;
  }

  Map toJson() {
    return {
      'figure': 'rectangle',
      'initialPointX': _left,
      'initialPointY': _top,
      'width': _width,
      'height': _height,
      'rgbaChan': _rgbaChan
    };
  }

  String toString() {
    return "Rect[Center(${_left + (_width/2)},${_top + (_height/2)}), Width(${_width}), Height(${_height})]";
  }
}

class EllipseFigure extends ColoredFigure {
  static final int _defaultR = 0;
  static final int _defaultG = 255;
  static final int _defaultB = 0;
  static final num _defaultA = 1.0;

  num _left, _top;
  num _diamX, _diamY;

  EllipseFigure(this._left, this._top, this._diamX, this._diamY, {int r, int g, int b, num a}) {
    int tempR = (r == null) ? _defaultR : r;
    int tempG = (g == null) ? _defaultG : g;
    int tempB = (b == null) ? _defaultB : b;
    num tempA = (a == null) ? _defaultA : a;
    setRGBAChannel(r: tempR, g: tempG, b: tempB, a: tempA);
  }

  EllipseFigure.fromJson(String jsonString) {
    Map data = JSON.decode(jsonString);
    _left = data['initialPointX'];
    _top = data['initialPointY'];
    _diamX = data['diamX'];
    _diamY = data['diamY'];
    _rgbaChan = JSON.decode(data['rgbaChan']);
  }

  num get left => _left;
  set left(num value) => _left = value;
  num get top => _top;
  set top(num value) => _top = value;

  num get diamX => _diamX;
  set diamX(num value) => _diamX = value;
  num get diamY => _diamY;
  set diamY(num value) => _diamY = value;

  num get centerX => _left + (_diamX / 2);
  num get centerY => _top + (_diamY / 2);
  num get radiusX => (_diamX / 2).abs();
  num get radiusY => (_diamY / 2).abs();

  bool checkPointInside(Point p) {
    //Ellipse equation, if val>1 the point is outside the ellipse
    num val = pow((p.x - this.centerX), 2) / pow(this.radiusX, 2) + pow((p.y - this.centerY), 2) / pow(this.radiusY, 2);
    if (val <= 1) return true; else return false;
  }

  Map toJson() {
    return {
      'figure': 'ellipse',
      'initialPointX': _left,
      'initialPointY': _top,
      'diamX': _diamX,
      'diamY': _diamY,
      'rgbaChan': _rgbaChan
    };
  }

  String toString() {
    return "Ellipse[Center(${_left + (_diamX/2)},${_top + (_diamY/2)}), Radius(${_diamX/2},${_diamY/2})]";
  }
}