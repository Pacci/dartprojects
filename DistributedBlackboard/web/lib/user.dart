part of blackboard;

class User {
  //User name
  String _username;
  //Options available to the user
  Map _optionsAvailable;

  //State of the user, defined by an option and a sub-option
  String _optionSelected;
  String _subOptionSelected;

  //Pointer position on the blackboard
  Point _pointer;

  User(this._username, this._optionsAvailable) {
    _optionSelected = "nothing";
    _subOptionSelected = "nothing";

    _pointer = new Point(-1, -1);
  }

  User.fromJson(String jsonString) {
    Map data = JSON.decode(jsonString);
    _username = data['username'];
    _optionsAvailable = JSON.decode(data['options']);

    _optionSelected = data['optionSelected'];
    _subOptionSelected = data['subOptionSelected'];

    _pointer = new Point(data['pointerPositionX'], data['pointerPositionY']);
  }

  Map toJson() {
    return {
      'username': _username,
      'options': _optionsAvailable,
      'optionSelected': _optionSelected,
      'subOptionSelected': _subOptionSelected,
      'pointerPositionX': _pointer.x,
      'pointerPositionY': _pointer.y
    };
  }

  String get username => _username;
  Map get options => _optionsAvailable;

  String get optSel => _optionSelected;
  String get subOptSel => _subOptionSelected;

  Point get pointerPosition => _pointer;
  set pointerPosition(Point p) => _pointer = p;
  
  //Change state accordingly to the available options
  //Return true if the state has changed, false otherwise
  bool changeState(String option, String subOption) {
    if (_optionsAvailable.containsKey(option)) {
      for (var subOpt in _optionsAvailable[option]["subOptions"]) {
        if(subOpt["name"] == subOption) {
          _optionSelected = option;
          _subOptionSelected = subOption;
          
          return true;
        }
      }
    }
    return false;
  }

  String toString() {
    return "User[UserName(${_username}, Option(${_optionSelected}), SubOption(${_subOptionSelected})]";
  }
}