import 'dart:convert';
import 'dart:math';
import 'dart:html';

import 'lib/logUnit.dart';
import 'lib/drawFunctions.dart';

final HOST = "127.0.0.1";
final PORT = 8081;
final SERVER = 'ws://${HOST}:${PORT}/ws';

WebSocket _ws;

var _onDataSubscriber;
var _onDownSubscriber;
var _onMoveSubscriber;
var _onUpSubscriber;

//User info
String _id;
String _userName;
List<Map> _optionsAvailable;

//HTML Elements
Element _container;
CanvasElement _canvas;

DivElement _log;
LogUnit _logUnit;

main() {
  _container = querySelector('#container');
  _canvas = querySelector('#screen');
  _canvas.style.border = "1px solid #000000";
  
  _log = querySelector('#log');
  _logUnit = new LogUnit(_log);
  
  _ws = new WebSocket(SERVER);

  _ws.onOpen.listen((Event e) {
    _logUnit.appendDebugMessage("Connection established..");
  });

  _onDataSubscriber = _ws.onMessage.listen((MessageEvent e) {
    handleFirstMessage(e);
  });

  _ws.onClose.listen((CloseEvent e) {
    _logUnit.appendDebugMessage("Connection closed..");
  });

  _ws.onError.listen((Event e) {
    _logUnit.appendDebugMessage("Connection Error!");
  });
}

handleFirstMessage(MessageEvent e) {
  Map info = JSON.decode(e.data);
  print(info);
  _id = info['id'];
  _userName = info['userInfo']['username'];
  _optionsAvailable = getOptionsAvailableAsList(info['userInfo']['options']);
  //DEBUG
  //print(_localUser.toString());

  for(Map element in _optionsAvailable) {
    String option = element['option'],
        suboption = element['suboption'],
        description = element['description'];
    
    ImageElement optionElement = 
        new ImageElement(src: "img/${option}_${suboption}.png", width: 20, height: 20)
        ..title = description
        ..style.border = "1px solid #000000"
        ..onClick.listen((_) {      
          Map mapToSend = {
            'id': _id,
            'eventType': 'changeState',
            'option': option,
            'suboption': suboption
          };
          _ws.sendString(JSON.encode(mapToSend));
      
          _logUnit.appendDebugMessage("OptionSelected: ${description}");
        });
    
    _container.append(optionElement);
  }
  //DEBUG
  //print(_optionsAvailable);

  //Change/Initialize handlers
  _onDataSubscriber.onData(handleServerState);
  _onDownSubscriber = _canvas.onMouseDown.listen((MouseEvent e) => sendEvent(e, "mouseDown"));
  _onMoveSubscriber = _canvas.onMouseMove.listen((MouseEvent e) => sendEvent(e, "mouseMove"));
  _onUpSubscriber = _canvas.onMouseUp.listen((MouseEvent e) => sendEvent(e, "mouseUp"));
}

sendEvent(MouseEvent e, String type) {
  //Get mouse point
  Point pg = e.page;
  Rectangle rec = _canvas.getBoundingClientRect();
  Point actualP = new Point(pg.x - rec.left, pg.y - rec.top);

  //Set the map to send to the server
  Map mapToSend = {
    'id': _id,
    'eventType': type,
    'pointX': actualP.x,
    'pointY': actualP.y
  };
  
  //Sending as Json string
  _ws.sendString(JSON.encode(mapToSend));
}

handleServerState(MessageEvent e) {
  //DEBUG
  //print(e.data);
  Map state = JSON.decode(e.data);

  //Update canvas
  CanvasRenderingContext2D ctx2D = _canvas.context2D;
  ctx2D.clearRect(0, 0, _canvas.width, _canvas.height);
  
  for (Map figure in state['drawn']) {
    drawFigure(ctx2D,figure);
  }

  for (Map figure in state['staging'].values) {
    drawFigure(ctx2D,figure);
  }
  
  for(Map pointer in state['users'].values) {
    drawCrossPointer(ctx2D, new Point(pointer['pointerPositionX'],pointer['pointerPositionY']));
  }
}

//Utils
List<Map> getOptionsAvailableAsList(Map optionsAvilable) {
  List<Map> result = new List<Map>();
  for(String opt in optionsAvilable.keys) {
    for (var subOpt in optionsAvilable[opt]['subOptions']) {
      result.add({'option': opt, 'suboption': subOpt['name'], 'description': subOpt['description']});
    }
  }
  
  return result;
}