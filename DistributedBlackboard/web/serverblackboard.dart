import 'dart:io';
import 'dart:math';
import 'dart:convert';
import 'dart:async';

import 'lib/blackboard.dart';

final HOST = "127.0.0.1";
final PORT = 8081;

Blackboard _bb;

final _defaultSizeX = 5000;
final _defaultSizeY = 5000;
final _defaultMaxUser = 2;

//Association 1-1 socket,id
Map<WebSocket, String> _socketUserMap;

StreamSubscription _ss;
//Periodic timer used to send the state of the blackboard
Timer _t;

main() {
  //Initialize the blackboard with the sizes and max user allowed
  _bb = new Blackboard(_defaultSizeX, _defaultSizeY, _defaultMaxUser);

  //Initialize the pairs WebSocket-StringId
  _socketUserMap = new Map<WebSocket, String>();

  runZoned(() {
    HttpServer.bind(HOST, PORT).then((server) {
      print("Server binded...");
      server.listen((HttpRequest req) {
        if (req.uri.path == '/ws') {
          // Upgrade a HttpRequest to a WebSocket connection.
          WebSocketTransformer.upgrade(req).then(handleSocket);
        }
      });
    });
  }, onError: (e) => print("An error occurred: ${e.toString()}"));
}

handleSocket(WebSocket s) {
  print("\n--------\nSomeone is trying to connect to the distributed blackboard...");
  new Future(() => _bb.connectUser()).then((String id) {
    if (id != null) {
      print("\nAccess allowed!\n--------\n");
      //Associate the WebSocket to the given Id
      _socketUserMap[s] = id;
      //Set the map to send to the user
      Map toSend = {
        'id': id,
        'userInfo': _bb.getById(id).toJson()
      };
      s.add(JSON.encode(toSend));

      //Activate the periodic timer (can be improved to send the updated state only in response of an event)
      if (_t == null || !_t.isActive) _t = new Timer.periodic(new Duration(milliseconds: 33), sendUpdateState);

      //Set message callback and other functions onError and onDone (when socket is closed)
      s.listen((msg) => handleUserEvent(s, msg))
          ..onError((e) {
            String idToRemove = _socketUserMap.remove(s);
            _bb.removeUser(idToRemove);
            if (_socketUserMap.isEmpty) _t.cancel();
          })
          ..onDone(() {
            print("User quit!");
            String idToRemove = _socketUserMap.remove(s);
            _bb.removeUser(idToRemove);
            if (_socketUserMap.isEmpty) _t.cancel();
          });
    } else {
      print("\nAccess denied!\n--------\n");
      s.close(WebSocketStatus.POLICY_VIOLATION, "Blackboard is full!");
    }
  });
}

sendUpdateState(Timer t) {
  for (WebSocket s in _socketUserMap.keys) {
    //s.add(_bb.stateToString());
    s.add(JSON.encode(_bb.stateAsJson()));
  }
}

/*
 * Event handler when receive a message from a user
 */
handleUserEvent(WebSocket s, msg) {
  Map data = JSON.decode(msg);

  //First check if the id corresponds to the websocket
  if (_socketUserMap[s] == data['id']) {
    //Different blackboard event handlers for different eventType fired by the users
    if (data['eventType'] == "changeState") {
      _bb.changeUserState(data['id'], data['option'], data['suboption']);
    } else if (data['eventType'] == "mouseDown") {
      _bb.mouseDownEvent(data['id'], new Point(data['pointX'], data['pointY']));
    } else if (data['eventType'] == "mouseMove") {
      _bb.mouseMoveEvent(data['id'], new Point(data['pointX'], data['pointY']));
    } else if (data['eventType'] == "mouseUp") {
      _bb.mouseUpEvent(data['id'], new Point(data['pointX'], data['pointY']));
    }
  }
}