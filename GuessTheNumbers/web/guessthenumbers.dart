import 'dart:html';
import 'dart:math';
import 'dart:async';

Element numberExtracted;
Element numberGuessed;
Element options;
ButtonElement startExtraction;
ButtonElement replayExtraction;

CanvasElement canvas;
CanvasRenderingContext2D ctx2D;
List<String> extractingValues;

InputElement rangeForNumbers;
OutputElement outputForNumbers;

InputElement rangeForMaxValues;
OutputElement outputForMaxValues;

final minNumber = 1;
final maxNumber = 9;
final step = 10;

int maxSecondsWait = 10;

main() {
  extractingValues = new List<String>();
  
  numberExtracted = querySelector("#numExtracted");
  
  canvas = querySelector("#drawNumExtracted");
  ctx2D = canvas.context2D;
  
  numberGuessed = querySelector("#numGuessed");
  options = querySelector("#optionParamenters");
  
  startExtraction = querySelector("#start");
  replayExtraction = querySelector("#replay");
  
  startExtraction.onClick.listen((_) => startTheGame());
  replayExtraction
      ..disabled = true
      ..onClick.listen((_) => reset());
  
  initializeOptions();
  initializeInput();
  //Update graphics with scheduleMicrotask
  scheduleMicrotask(updateGraphics);
}

initializeOptions()
{
  /*
   * Creates the element for the optionParamenters.
   * Two sliders for setting the number of extractions and the value range of number extracted.
   */
  rangeForNumbers = new InputElement()
  ..type = "range"
  ..min = "$minNumber"
  ..max = "$maxNumber"
  ..value = "$minNumber"
  ..onInput.listen((_) {
    outputForNumbers.value  = rangeForNumbers.value;
    scheduleMicrotask(updateNumberInputElements);
    scheduleMicrotask(updateGraphics);
  });
  
  outputForNumbers = new OutputElement()
  ..name = "outputForRange"
  ..value = rangeForNumbers.value;
  
  rangeForMaxValues = new InputElement()
  ..type = "range"
  ..min = "${step*minNumber}"
  ..max = "${step*maxNumber}"
  ..step = "$step"
  ..value = "${step*minNumber}"
  ..onInput.listen((_) {
    outputForMaxValues.value  = rangeForMaxValues.value;
    scheduleMicrotask(updateMaxValueInputElements);
  });
    
  outputForMaxValues = new OutputElement()
  ..name = "outputForMax"
  ..value = rangeForMaxValues.value;
  
  /*
   * Appending the new created elements in the DOM
   */
  options.append(rangeForNumbers);
  options.append(outputForNumbers);
  options.append(rangeForMaxValues);
  options.append(outputForMaxValues);
}

reset()
{
  rangeForNumbers.value = rangeForNumbers.min;
  rangeForMaxValues.value = rangeForMaxValues.min;
  new Future( () => initializeInput())
  .then( (_) => updateGraphics())
  .then( (_) { 
    startExtraction.disabled = false;
    rangeForNumbers.disabled = false;
    rangeForMaxValues.disabled = false;
    replayExtraction.disabled=true;
  });
}

initializeInput()
{
  numberGuessed.nodes.clear();
  numberGuessed.nodes.add(new InputElement()
      ..type = "number"
      ..value = "$minNumber"
      ..min = "1"
      ..max = rangeForMaxValues.value);
  
  extractingValues.clear();
  extractingValues.add("!");
}

startTheGame()
{
  startExtraction.disabled = true;
  rangeForNumbers.disabled = true;
  rangeForMaxValues.disabled = true;
  
  List<Future<int>> futureElements = new List<Future<int>>();
  
  for(int i=0; i<numberGuessed.nodes.length; i++)
  {
    Future<int> fi = getFutureNumber().then((int valueExtracted) {
      numberGuessed.nodes[i].disabled = true;
      extractingValues[i] = "$valueExtracted";
      print("Extracted a number, value is: $valueExtracted");
      scheduleMicrotask(updateGraphics);
    });
      
    futureElements.add(fi);
  }
  
  Future.wait(futureElements).then((_) {
    print("Extraction concluded!");
    replayExtraction.disabled = false;
    scheduleMicrotask(updateGraphics);
  });
}

Future<int> getFutureNumber()
{
  int maxValue = int.parse(rangeForMaxValues.value);
  int extractedValue = new Random().nextInt(maxValue);
  return new Future.delayed(new Duration(seconds:(new Random().nextInt(maxSecondsWait))),
      () {return extractedValue+1;});
}

updateNumberInputElements()
{
  int newValue = int.parse(rangeForNumbers.value);
  int diff = newValue - numberGuessed.nodes.length;
  
  if(diff > 0)
  {
    //Have to add some input number elements
    for(int i=0; i<diff; i++)
    {
      numberGuessed.nodes.add(new InputElement()
        ..type = "number"
        ..value = "$minNumber"
        ..min = "1"
        ..max = rangeForMaxValues.value);
      extractingValues.add("!");
    }
  }
  else
  {
    //Have to remove some input number elements
    diff = -1*diff;
    for(int i=0; i<diff; i++)
    {
      numberGuessed.nodes.removeLast();
      extractingValues.removeLast();
    }
  }
}

updateMaxValueInputElements()
{
  for(Node el in numberGuessed.nodes)
  {
    if(el is InputElement)
    {
      if(int.parse(el.value) > int.parse(rangeForMaxValues.value))
       el.value = rangeForMaxValues.value;
      el.max = rangeForMaxValues.value;
    }
  }
}


//Draw function
updateGraphics()
{
  String style;
  ctx2D.font = "60pt Calibri";
  ctx2D.textAlign = "center";
  int numCircle = int.parse(rangeForNumbers.value);
  int radius = 20;
  
  canvas.width = radius*2*numCircle;
  canvas.height = radius*2;
  
  ctx2D..fillStyle = "white";
  ctx2D.fillRect(0, 0, canvas.width, canvas.height);
  
  for(int i=0; i<numCircle; i++)
  {
    var centerX = i*(radius*2) + radius;
    var centerY = radius;
    
    
    if(extractingValues[i] == "!")
    {
      ctx2D..beginPath()
      ..fillStyle = "lightblue"
      ..arc(centerX, centerY, radius, 0, 2*PI)
      ..fill();
    }
    else if(extractingValues[i] == numberGuessed.nodes[i].value)
    {
      ctx2D..beginPath()
      ..fillStyle = "green"
      ..arc(centerX, centerY, radius, 0, 2*PI)
      ..fill();
    }
    else
    {
      ctx2D..beginPath()
        ..fillStyle = "red"
        ..arc(centerX, centerY, radius, 0, 2*PI)
        ..fill();
    }
  }
  
  for(int i=0; i<numCircle; i++)
  {
      var centerX = i*(radius*2) + radius;
      var centerY = radius;
      
      ctx2D..fillStyle = "black"
        ..font = "20pt Calibri"
        ..textAlign = "center"
        ..fillText(extractingValues[i], centerX-2, centerY+8);
    }
}