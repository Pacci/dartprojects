import "dart:html";
import "dart:convert";
import "dart:async";

final String server = "127.0.0.1:8081";

List<String> folderHierarchy;

ButtonElement buttonRefresh;
ButtonElement buttonSave;
SpanElement currentFolder;
DivElement folderList;
DivElement fileList;
SpanElement currentFile;
TextAreaElement fileContent;
StreamSubscription<KeyboardEvent> textAreaSubscription;

void main() {
  buttonRefresh = querySelector("#refresh");
  buttonSave = querySelector("#save");
  currentFolder = querySelector("#currentFolder");
  folderList = querySelector("#folderList");
  fileList = querySelector("#fileList");
  currentFile = querySelector("#currentFile");
  fileContent = querySelector("#fileContent");
  
  folderHierarchy = new List<String>();

  buttonSave.onClick.listen(saveFileContent);
  buttonRefresh.onClick.listen(refreshFolder);
}

refreshFolder(MouseEvent e) {
  currentFile.text = "";
  
  fileContent.value = "";
  if(textAreaSubscription!=null)
    textAreaSubscription.cancel();
  
  buttonSave.disabled = true;
  buttonRefresh.text = "Refresh";
  
  folderHierarchy.clear();
  currentFolder.text = currentFolderPath();
  reset();
}

saveFileContent(MouseEvent e) {
  String url = "http://${server}/${currentFile.text}";
  HttpRequest req = new HttpRequest();
  req.open("POST", url);
  req.send(fileContent.value);
  
  buttonSave.disabled = true;
}

reset() {
  String url = "http://${server}/folderContent";
  print(url);

  HttpRequest.getString(url).then((String content) {
    Map data = JSON.decode(content);
    updateFolderList(data["directories"]);
    updateFileList(data["files"]);
    
    updateCurrentFolderField();
  });
}

//Folder

updateCurrentFolderField() {
  currentFolder.text = "";
  for (String element in folderHierarchy) {
    if (element != "root") currentFolder.text = currentFolder.text + "${element}/";
  }
}

String currentFolderPath() {
  String result = "";
  for (String element in folderHierarchy) result += "/${element}";
  return result;
}

loadFolderList(String folderName) {
  if (folderName != null) {
    //set url for the resources
    var getUrl = "http://${server}${currentFolderPath()}/${folderName}/folderContent";
    print(getUrl);

    HttpRequest.getString(getUrl).then((String content) {
      Map data = JSON.decode(content);
      updateFolderList(data["directories"]);
      updateFileList(data["files"]);

      folderHierarchy.add(folderName);
      updateCurrentFolderField();
    });
  }
}

updateFolderList(List newFolderList) {
  folderList.children.clear();

  for (String folder in newFolderList) {
    var newElement = new LIElement();
    newElement.text = folder;
    newElement.onClick.listen((onEvent) {
      loadFolderList(folder);
    });
    folderList.append(newElement);
  }
  //Adding parent folder link
  var newElement = new LIElement();
  newElement.text = "..";
  newElement.onClick.listen((onEvent) {
    if (folderHierarchy.length > 0) 
      folderHierarchy.removeLast();
    String folderToSearch;
    if (folderHierarchy.length == 0) 
      reset();
    else 
      loadFolderList(folderHierarchy.removeLast());
  });
  folderList.append(newElement);
}

//File

updateFileList(List newFileList) {
  fileList.children.clear();

  for (String file in newFileList) {
    var newElement = new LIElement();
    newElement.text = file;
    newElement.onClick.listen((onEvent) {
      loadFileContent(file);
    });
    fileList.append(newElement);
  }
}

//File Content
loadFileContent(String filePath) {
  if (filePath != null) {
    //set url for the resources
    var getUrl = "http://${server}/${currentFolder.text}${filePath}/fileContent";
    print(getUrl);

    HttpRequest.getString(getUrl).then((String content) {
      buttonSave.disabled = true;
      Map data = JSON.decode(content);
      
      fileContent.value = data["content"];
      
      /*
       * Some issue on key input event handling:
       * http://stackoverflow.com/questions/14433156/respond-immediately-to-textarea-changes-in-dart
       */
      textAreaSubscription = fileContent.onInput.listen((_) => buttonSave.disabled = false);
      currentFile.text = currentFolder.text + "${filePath}";
    });
  }
}
