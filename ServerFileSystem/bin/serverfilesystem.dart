import "dart:io";
import "dart:async";
import "dart:convert";


//final HOST = InternetAddress.LOOPBACK_IP_V4;
final HOST = "127.0.0.1";
final PORT = 8081;

void main() {
  HttpServer.bind(HOST, PORT).then((HttpServer server) {
    help();
    server.listen(handleRequest);
  });
}

help() {
  print("""

FileSystem Server!

Listening on ${HOST}:${PORT}... 
""");
}

handleRequest(HttpRequest request) {
  String path = request.requestedUri.path;
  print("Path: $path");
  //The client is requesting the list of the folders/files in a specified res/[path]
  if (path.endsWith("/folderContent") && request.method == "GET") 
    serveFolderContent(request, "res$path");
  //The client is requesting file content in a specified res/[path]
  else if (path.endsWith("/fileContent") && request.method == "GET") 
    serveFileContent(request, "res$path");
  //The client is requesting to save the content of a file
  else if(request.method == "POST")
    handlePost(request);
  //Used in the client side
  else if(path == '/')
    serveStaticFile(request,"web/clientfilesystem.html");
  else if(path.startsWith('/web/'))
    serveStaticFile(request,path.substring(1));
  //Send error
  else
    errorPageHandler(request.response);
//  path = (request.requestedUri.path == "/" ? "web/index.html" : "res/${request.requestedUri.path}");
//
//  print(request.uri);
//
//  if ((path.endsWith(".html") || path.endsWith(".css") || path.endsWith(".js") || path.endsWith(".dart") || path.endsWith(".json") || path.endsWith(".ico")) && request.method == "GET") serveStaticFile(request, path); else serveFolderContent(request, path);
}

handlePost(HttpRequest req) {
  HttpResponse res = req.response;
  print('${req.method}: ${req.uri.path}');

  addCorsHeaders(res);

  req.listen((List<int> buffer) {
    res.write("Received Data");
    res.close();
    String newContent = new String.fromCharCodes(buffer);
    print(newContent);
    
    File f = new File("res/${req.uri.path}");
    f.exists().then((exist) {
      if(exist) {
        IOSink sink = f.openWrite();
        sink.write(newContent);
        sink.close();
      }
    });
  });
}

serveStaticFile(HttpRequest request, String path) {
  File indexFile = new File(path);

  indexFile.exists().then((result) {
    if (result) {
      print("Sending file ${indexFile.path}");

      indexFile.openRead().pipe(request.response).catchError((e) {});
    } else {
      print("File ${indexFile.path} not present");
      errorPageHandler(request.response);
    }
  });
}

serveFolderContent(HttpRequest request, String path) {
  String folderPath = path.substring(0, path.length - "/folderContent".length);
  print("Serving list of folders! StartingPath: ${folderPath}; method: ${request.method}");
  getContentListAsJSon(request.response, folderPath);
}

serveFileContent(HttpRequest request, String path) {
  String filePath = path.substring(0, path.length - "/fileContent".length);
  print("Serving file content! Path: ${filePath}; method: ${request.method}");
  getFileContentAsJSon(request.response, filePath);
}


Future<List<FileSystemEntity>> dirContents(Directory dir) {
  var files = <FileSystemEntity>[];
  var completer = new Completer();
  var lister = dir.list(recursive: false);
  lister.listen((file) => files.add(file),
  onDone: () => completer.complete(files));
  return completer.future;
}

getContentListAsJSon(HttpResponse res, String path) {
  addCorsHeaders(res);
  Directory dir = new Directory(path);

  dir.exists().then((bool validPath) {
    if (validPath) {

      Map<String, List> fileDirMap = new Map<String, List>();
      List<String> dirList = new List<String>();
      List<String> fileList = new List<String>();

      dirContents(dir).then((fileEntityList) {
        List<Future> toWait = new List<Future>();
        for (FileSystemEntity fileEntity in fileEntityList) {
          toWait.add(FileSystemEntity.type(fileEntity.path));
        }

        Future.wait(toWait).then((listType) {
          for (int i = 0; i < listType.length; i++) {
            if (listType[i] == FileSystemEntityType.DIRECTORY) dirList.add(drop(fileEntityList[i].path)); else if (listType[i] == FileSystemEntityType.FILE) fileList.add(drop(fileEntityList[i].path));
          }

          fileDirMap["directories"] = dirList;
          fileDirMap["files"] = fileList;

          String jsonString = JSON.encode(fileDirMap);
          res.write(jsonString);
          res.close();
        });
      });
    }
    else {
      errorPageHandler(res);
    }
  });
}

getFileContentAsJSon(HttpResponse res, String filePath) {
  addCorsHeaders(res);
  File file = new File(filePath);

  file.exists().then((bool validFile) {
      if (validFile) {
        Map<String, String> fileContent = new Map<String, String>();
        file.readAsString().then((String content) {
          fileContent["content"] = content;
          String jsonString = JSON.encode(fileContent);
          res.write(jsonString);
          res.close();
        }); 
      }
      else {
        errorPageHandler(res);
      }
  });
}

//Utils functions
String drop(String input) {
  return input.replaceAll(r'\',"/").split('/').last;
}

addCorsHeaders(HttpResponse res) {
  res.headers.add("Access-Control-Allow-Origin", "*, ");
  res.headers.add("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
  res.headers.add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
}

errorPageHandler(HttpResponse res) {
  res..statusCode = HttpStatus.NOT_FOUND
     ..write('Not found')
     ..close();
}
